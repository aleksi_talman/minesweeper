import haravasto as ha
import random
import time
import datetime

spritekansio = "C:/Users/aleks/Documents/Aleksi/Koulu/Lipasto/Ohjelmointi/OA/Lopputyö/spritet"
lopputyökansio = "C:/Users/aleks/Documents/Aleksi/Koulu/Lipasto/Ohjelmointi/OA/lopputyö"

tila = {
    "nakyvakentta": [],
    "piirtokentta": [],
}

pelikentta = []
jaljella = []

tilastokirja = {
    "leveys": 0,
    "korkeus": 0,
    "miinoja": 0,
    "pelintulos": " ",
    "aika": 0,
    "loppuaika": 0,
}

#ajanlasku
def aloita_ajanlasku():
    tilastokirja["aika"] = time.time()

def lopeta_ajanlasku():
    lopetusaika = time.time()
    kokonaisaika = (lopetusaika - tilastokirja["aika"])/60
    if kokonaisaika >= 1.0:
        tilastokirja["loppuaika"] = ("{:.3f} minuuttia".format(kokonaisaika))
    elif kokonaisaika < 1.0:
        kokonaisaika_sekuntti = kokonaisaika*60
        tilastokirja["loppuaika"] = ("{:.3f} sekuntia".format(kokonaisaika_sekuntti))

#voitto, havio tilastojen tallennus yms.
def pelin_voitto():
    lopputulos()
    tilastokirja["pelintulos"] = "voitto"
    tallenna_tilastoja()
    lopeta_ajanlasku()
    time.sleep(1)
    print("Voitit pelin!")
    print("Peli kesti {}".format(tilastokirja["loppuaika"]))
    main()

def lopeta_peli():
    time.sleep(3)
    ha.lopeta()

def pelin_havio():
    lopputulos()
    tilastokirja["pelintulos"] = "havio"
    lopeta_ajanlasku()
    tallenna_tilastoja()
    time.sleep(1)
    print("BOOM! Osuit miinaan ja hävisit pelin!")
    print("Peli kesti {}".format(tilastokirja["loppuaika"]))
    main()

def lopputulos():
    for y, rivi in enumerate(tila["nakyvakentta"]):
        for x, ruutu in enumerate(rivi):
            tila["nakyvakentta"][y][x] = tila["piirtokentta"][y][x]

#tilastoihin liittyvät funktiot
def tilastot(tiedosto="tiedot.txt"):
    print("TILASTOSI:")
    print("Viimeisin pelisi:")
    print(nayta_tulokset(tiedosto))
    print("Kirjoita t, jos haluat takaisin alkuvalikkoon")
    takaisin_valinta = input().strip().lower()
    while takaisin_valinta != "t":
        print("Syötä oikea kirjain (t)!")
    else:
        alkuvalikko()

def nayta_ohjeet():
    print("OHJEISTUS MIINAHARAVAAN!")
    print("Valitse aluksi kentän leveys, korkeus ja miinojen määrä ohjelman ohjeistuksen mukaan")
    print("Peli ruutu avautuu ja voit aloittaa pelin klikkailemalla ruutuja")
    print("Tarkoituksena on saada kaikki ruudut avattua ja miinat jättää koskemattomaksi jotta peli voidaan voittaa")
    print("Pelin voi jättää kesken sammutamalla ikkunan oikeasta yläkulmasta")
    print("Peli myös sen päätyttyä sammutetaan oikeasta yläkulmasta")
    print("Kirjoita t, jos haluat takaisin alkuvalikkoon")
    takaisin_valinta = input()
    while takaisin_valinta != "t":
        print("Syötä oikea kirjain (t)!")
    else:
        alkuvalikko()

#tilasto jutut
def tallenna_tilastoja(viimeisin="tiedot.txt", kaikki="kaikki_tiedot.txt"):
    aika_nyt = time.localtime()
    peliaika = time.strftime("%H:%M:%S", aika_nyt)
    pelipaiva = datetime.date.today()
    with open(viimeisin, "w") as file:
        file.truncate(0)
        file.close()
    try:
        with open(viimeisin, "a") as tallennus:
            tallennus.write("Peli pelattiin: {} {}, Pelin kesto: {}. Ruudukko oli {} ruutua korkea ja {} ruutua leveä. Miinoja oli kentällä {} kpl. Pelin lopputulos oli: {}".format(peliaika, pelipaiva, tilastokirja["loppuaika"], tilastokirja["korkeus"], tilastokirja["leveys"], tilastokirja["miinoja"],  tilastokirja["pelintulos"]))
    except IOError:
        pass
    try:
        with open(kaikki, "a") as tallennus:
            tallennus.write("Peli pelattiin: {} {}, Pelin kesto: {}. Ruudukko oli {} ruutua korkea ja {} ruutua leveä. Miinoja oli kentällä {} kpl. Pelin lopputulos oli: {}\n".format(peliaika, pelipaiva, tilastokirja["loppuaika"], tilastokirja["korkeus"], tilastokirja["leveys"], tilastokirja["miinoja"],  tilastokirja["pelintulos"]))
    except IOError:
        pass

def nayta_tulokset(data):
    try:
        with open(data, "r") as lahde:
            for rivi in lahde.readlines():
                tiedot = rivi.lower()
    except IOError:
        pass
    return tiedot

def tulvataytto(alku_y, alku_x):
    ''' kutsuattaessa tutkii halutun x, y koordinaattiparin ympäriltä ruutuja
    jonka jälkeen lisää ruutuja näkyvään kenttään (eli avaa ruudut)
    jos ruudut ovat sopivia avattavaksi'''
    kartta = [(alku_y, alku_x)]
    tarkastetut_ruudut = []
    while len(kartta) > 0:
        pari = kartta.pop()
        y = pari[0]
        x = pari[1]
        rivit = [y-1, y, y+1]
        sarakkeet = [x-1, x, x+1]
        tila["nakyvakentta"][y][x] = tila["piirtokentta"][y][x]
        for rivi in rivit:
            for ruutu in sarakkeet:
                if 0 <= rivi < len(tila["nakyvakentta"]):
                    if 0 <= ruutu < len(tila["nakyvakentta"][0]):
                        if (rivi, ruutu) not in tarkastetut_ruudut:
                            if tila["piirtokentta"][rivi][ruutu] == "0":
                                kartta.append((rivi, ruutu))
                                tarkastetut_ruudut.append((rivi, ruutu))
                            elif tila["piirtokentta"][rivi][ruutu] != "0" and tila["piirtokentta"][rivi][ruutu] != "x":
                                tila["nakyvakentta"][rivi][ruutu] = tila["piirtokentta"][rivi][ruutu]
                                            
#miinoitus, miinojen lukumaara ja numeroruutujen piirto
def miinoita(lukumaara, kentta, vapaat_ruudut):
    """
    Asettaa kentälle N kpl miinoja satunnaisiin paikkoihin
    """
    miinat= []
    while lukumaara:
        y, x = random.choice(vapaat_ruudut)
        if kentta[y][x] != "x":
            kentta[y][x] = "x"
            vapaat_ruudut.remove([y, x])
            miinat.append([y, x])
            lukumaara -= 1

def ruutujen_numerointi(lista):
    """
    Tutkii kaikki ruudut läpi, eli montako miinaa on kunkin ruudun ympärillä.
    sijoittaa lukuarvon stringinä listaan jotta se voidaan sitten avatessa piirtää
    """
    for y, rivi in enumerate(lista):
        for x, ruutu in enumerate(rivi):
            if ruutu != "x":
                miinalkm = miina_laskuri(x, y)
                if miinalkm >= 0:
                    lista[y][x] = str(miinalkm)
    
def miina_laskuri(x, y):
    '''laskee montako miinaa on kunkin ruudun ympärillä'''
    miinoja = 0
    rivit = [y-1, y, y+1]
    sarakkeet = [x-1, x, x+1]
    for rivi in rivit:
        for ruutu in sarakkeet:
            if 0 <= rivi < len(tila["piirtokentta"]) and 0 <= ruutu < len(tila["piirtokentta"][0]):
                if tila["piirtokentta"][rivi][ruutu] == "x":
                    miinoja += 1
    return miinoja

#kasittelijat
def kasittele_hiiri(x, y, nappi, muokkausnapit):
    """
    Tätä funktiota kutsutaan kun käyttäjä klikkaa sovellusikkunaa hiirellä
    kutsuu ruutujen avaamis funktiota
    """
    if nappi == ha.HIIRI_VASEN:
        koord_x = int(x/40)
        koord_y = int(y/40)
        avaa_ruutu(koord_x, koord_y)
    elif nappi == ha.HIIRI_OIKEA:
        pass
    elif nappi == ha.HIIRI_KESKI:
        pass

def piirra_kentta(): 
    '''Tätä kutsuttaessa pelinäkymä päivittyy'''
    ha.tyhjaa_ikkuna()
    ha.piirra_tausta()
    ha.aloita_ruutujen_piirto()
    for y in range(len(tila["nakyvakentta"])):
        for x in range(len(tila["nakyvakentta"][0])):
            ruutu = tila["nakyvakentta"][y][x]
            ha.lisaa_piirrettava_ruutu(ruutu, (x*40), (y*40))
    ha.piirra_ruudut()

#pelikentänluonti
def pelikentänluonti(leveys, korkeus):
    ''' luo pelikentän ja listan myös vapaille ruuduille'''
    pelikentta = []
    piirtokentta = [] 
    for rivi in range(korkeus):
        pelikentta.append([])
        piirtokentta.append([])
        for sarake in range(leveys):
            pelikentta[-1].append(" ")
            piirtokentta[-1].append(" ")
    tila["nakyvakentta"] = pelikentta
    tila["piirtokentta"] = piirtokentta
    for y in range(korkeus):
        for x in range(leveys):
            jaljella.append([y, x])

#ruutujen avaus
def avaa_ruutu(alku_x, alku_y):
    '''funktio jota kutsutaan klikatessa, avaa ruudun käyttäjän klikkaamaan kohtaan ja mahdollisesti tulvatäyttää'''
    if tila["nakyvakentta"][alku_y][alku_x] == " ":
        if tila["piirtokentta"][alku_y][alku_x] == "x":
            pelin_havio()
        elif tila["piirtokentta"][alku_y][alku_x] == "0":
            tulvataytto(alku_y, alku_x)
            piirra_kentta()
            tarkista_voitto()
        elif int(tila["piirtokentta"][alku_y][alku_x]) > 0:
            tila["nakyvakentta"][alku_y][alku_x] = tila["piirtokentta"][alku_y][alku_x]
            piirra_kentta()
            tarkista_voitto()

#voiton tarkistus
def tarkista_voitto():
    '''tarkistaa onko voittoehto täyttynyt'''
    tyhjät = 0
    for i in tila["nakyvakentta"]:
        tyhjät += i.count(" ")
        if tyhjät == tilastokirja["miinoja"]:
            pelin_voitto()
        else:   
            pass
           
#alkuvalikko yms
def alkuvalikko():
    '''kysyy mitä pelaaja haluaa tehdä sovelluksen käynnistäessään'''
    valinnat = ["a", "t", "s", "o"]
    print("Valinnat: Aloita peli (a), tarkastele tilastoja (t), sulje peli (s), näytä ohjeet(o)")
    valinta = input()
    while str(valinta) != valinnat:
        if valinta.lower().strip() == valinnat[0]:
            print("Aloitetaan!")
            kysy_kayttajalta()
        elif valinta.lower().strip() == valinnat[1]:
            tilastot()
        elif valinta.lower().strip() == valinnat[2]:
            pelin_sulku()
        elif valinta.lower().strip() == valinnat[3]:
            nayta_ohjeet()
    else:
        print("Väärä valinta! Yritä uudestaan")
        alkuvalikko()

def pelin_sulku():
    try:
        print("Kiitos pelaamisesta!")
        ha.lopeta()
    except AttributeError:
        ha.lopeta()

def kysy_kayttajalta():
    '''kysyy kayttajalta haluamansa kentän koon sekä miinojen määrän. 
    Ohjelma ilmoittaa käyttäjälle mitkä ovat suotuisat koot kentälle
    '''
    print("Syötä haluamasi kentän koko ruuduissa, sekä miinojen lukumäärä")
    print("Maksimi korkeus on 25 ruutua ja maksimi leveys 47 ruutua")
    print("Valitse miinojen määrä itse. Huomaa että mitä enemmän miinoja, sitä haastavampi peli on")
    korkeus = int(input("korkeus: "))
    leveys = int(input("leveys: "))
    miinoja = int(input("miinoja: "))
    while miinoja <= (leveys*korkeus) and korkeus < 26 and leveys < 48:
        while korkeus < 26 and leveys < 48:
            if korkeus < 26 and leveys < 48:
                tilastokirja["korkeus"] = korkeus
                tilastokirja["leveys"] = leveys
                tilastokirja["miinoja"] = miinoja
                aloita(korkeus, leveys, miinoja)
            elif korkeus < 26 and leveys > 47:
                print("Kenttäsi on liian leveä!")
            elif korkeus > 25 and leveys > 47:
                print("Kenttäsi on liian iso!")
            elif korkeus > 25 and leveys < 48:
                print("Kenttäsi on liian korkea!")
            elif NameError or TypeError:
                print("Syötä kentän koko kokonaislukuina!")
    else:
        print("Miinoja on enemmän kuin kenttä on suuri!")

def main():
    print("Tervetuloa miinaharavaan!")
    print("Mitä haluat tehdä?")
    alkuvalikko()

def aloita(korkeus, leveys, miinoja):
    aloita_ajanlasku()
    ikkuna_korkeus = korkeus*40
    ikkuna_leveys = leveys*40
    ha.lataa_kuvat(spritekansio)
    ha.luo_ikkuna(ikkuna_leveys, ikkuna_korkeus, taustavari=(240, 120, 25, 45))
    pelikentänluonti(leveys, korkeus)
    miinoita(miinoja, tila["piirtokentta"], jaljella)
    ruutujen_numerointi(tila["piirtokentta"])
    ha.aseta_piirto_kasittelija(piirra_kentta)
    ha.aseta_hiiri_kasittelija(kasittele_hiiri)
    ha.aloita()

if __name__ == "__main__":
    tilastokirja = {
    "leveys": 0,
    "korkeus": 0,
    "miinoja": 0,
    "pelintulos": " ",
    "aika": 0,
    "loppuaika": 0,
    }
    main()